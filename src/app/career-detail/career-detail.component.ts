import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router, NavigationEnd } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ConfigService } from 'app/services/config.service';
import { SearchListService } from 'app/services/search-list.service';
import { CareerDetailService } from 'app/services/career-detail.service';
import { LoginDialogComponent } from 'app/core/dialogs/login-dialog/login-dialog.component';
import { NotesDialogComponent } from 'app/core/dialogs/notes-dialog/notes-dialog.component';
import { NeedToLoginDialogComponent } from 'app/core/dialogs/need-to-login-dialog/need-to-login-dialog.component';
import { JobTitlesDialogComponent } from 'app/core/dialogs/job-titles/job-titles.component';
import { FavoritesService } from 'app/services/favorites.service';
import {GoogleAnalyticsService} from '../services/google-analytics.service';
import { ScoreDialogComponent } from 'app/core/dialogs/score-dialog/score-dialog.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { ScoreService } from '../services/score.service';
import { UtilityService } from '../services/utility.service';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-career-detail',
  templateUrl: './career-detail.component.html',
  styleUrls: ['./career-detail.component.scss']
})

export class CareerDetailComponent implements OnInit, AfterViewInit, OnDestroy {
  navigationSubscription;
  mcId: any;
  needLoadData;
  mcIdLastTwoChar = '';
  mcIdTrimmed = '';
  militaryCareer: any;
  whatTheyDo: any;
  trainingProvided: any;
  trainingProvidedList: any;
  hotJob: any;
  serviceOffering: any;
  serviceOfferingCompositeScore: any;
  otherTitles: any;
  otherCareerTitles: String;
  armyScore: any;
  armyScoreFlag= false;
  featuredProfile: any;
  profile: any;
  skillRating: any;
  parentSkillRating: any;
  parentMilitaryCareer: any;
  careerPathway: any;
  helpfulAttributes: any;
  recentlyViewed: any;
  searchList;
  searchListItem;
  allFilter;
  secondAllFilter;
  imageUrl = '';
  armyServiceOffering;
  marineServiceOffering;
  navyServiceOffering;
  airForceServiceOffering;
  coastGuardServiceOffering;
  nationalGuardServiceOffering;
  gotServiceOffering = false;
  armyScoreData;
  marineScoreData;
  navyScoreData;
  airForceScoreData;
  coastGuardScoreData;
  nationalGuardScoreData;
  snapshotQuestionList;
  careerDescription: any;
  careerTitle: any;
  showMore = true;
  showMore2 = true;
  showMore3 = true;
  showMore4 = true;
  navyScores = false;
  airForceScores = false;
  detailSearchListInterval;
  serviceImage: any;
  shortenDescription: any;
  compositeScores: any;
  slideConfig = {
    'slidesToShow': 1,
    'slidesToScroll': 1,
    'dots': false,
    'infinite': false,
    'autoplay': true,
    prevArrow: '<button type="button" class="slick-prev" role="button">' +
    '<img src="assets/images/arrow-left-red.png"></button>',
    nextArrow: '<button type="button" class="slick-next" role="button">' +
    '<img src="assets/images/arrow-right-red.png"></button>'
  };
  profileSlideConfig = {
    'slidesToShow': 1,
    'slidesToScroll': 1,
    'dots': true,
    'infinite': false,
    'autoplay': false,
    'autoplaySpeed': 5000,
    'pauseOnHover': true,
    'arrows' : false,
  };
  rSvcId;
  rMcId;
  rMocId;
  
  linguistCareers = ['0080.00', '0083.00', '0076.00', '0076.02', '0077.00', '0077.02', '0078.00', '0079.00'];
  showLinguistVideo = false;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _config: ConfigService,
    private _careerDetailService: CareerDetailService,
    private _searchService: SearchListService,
    private _favoritesService: FavoritesService,
    private _dialog: MatDialog,
    private _spinner: NgxSpinnerService,
    private _meta: Meta,
    private _titleTag: Title,
    private _scoreService: ScoreService,
    private _googleAnalyticsService: GoogleAnalyticsService,
    private _utility: UtilityService,
  ) {
          // Subscribe to router event Navigation End so can refresh data after log in
          this.navigationSubscription = this._router.events.subscribe(event => {
            if (event instanceof NavigationEnd && event.urlAfterRedirects.indexOf('/career-detail') > -1) {
              this.needLoadData = true;
              this._activatedRoute.params.subscribe(routeParams => {
                this.loadData(routeParams.mcId);
              });
            }
          });
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    window.scrollTo(0, 0);
    this.detailSearchListInterval = null;
  }
  
  isLinguistCareer() {
    var isFound = false;
    for (var i = 0; i < this.linguistCareers.length; i++) {
        if (this.linguistCareers[i] === this.mcId) {
            isFound = true;
            break;
        }
    }
    this.showLinguistVideo = isFound;
}

  ga(param) {
    this._googleAnalyticsService.trackClick(param);
  }

  isLoggedIn() {
    return this._config.isLoggedIn();
  }

  loadData(mcId) {
    // check this to make sure it is after the Navigation End and not just Activated Route Subscription 
    if (this.needLoadData === true) {
      this.mcId = mcId;
      this.needLoadData = false;
      this.mcIdLastTwoChar = this.mcId.substr(this.mcId.length - 2);
      this.mcIdTrimmed = this.mcId.slice(0, -3);
    
    this.isLinguistCareer();

      const searchObject = this._searchService.getSearchList();
      const self = this;
      if (!searchObject) {
        if (!this.detailSearchListInterval) {
          this.detailSearchListInterval = setInterval(function () {
            if (!self._searchService.getProcessingSearchList()) {
              clearInterval(self.detailSearchListInterval);
              self.detailSearchListInterval = null;
              self.searchList = self._searchService.searchListObject;
              self.setSearchList();
            }
          }, 200);
        }
      } else {
        this.searchList = searchObject;
        this.setSearchList();
      }

      if (this.isLoggedIn()) {
        this._scoreService.getScore().subscribe(data => {
          this.compositeScores = data;
        });
      }

      this._careerDetailService.getMilitaryCareer(this.mcId).subscribe(data => {
        this.militaryCareer = data;
        this.refreshData();
      });

      this._careerDetailService.getCareerPathway(this.mcId).subscribe(data => {
        this.careerPathway = data;
      });

      this._careerDetailService.getOtherTitles(this.mcId).subscribe(data => {
        this.otherTitles = data;
        this.setCareerTitles();
      });

      this._careerDetailService.getWhatTheyDo(this.mcId).subscribe(data => {
        this.whatTheyDo = data;
      });

      this._careerDetailService.getTrainingProvided(this.mcId).subscribe(data => {
        this.trainingProvided = data;
        this.trainingProvidedList = this.trainingProvided[0].list;
      });

      this._careerDetailService.getArmyScores(this.mcId).subscribe(data => {
        this.armyScore = data;
        if (data.length > 0) {
          this.armyScoreFlag = true;
        }
        this.refreshData();
      });

      this._careerDetailService.getServiceOffering(this.mcId).subscribe(data => {
        this.serviceOffering = data;
        this.refreshData();
      });

      this._careerDetailService.getHelpfulAttributes(this.mcId).subscribe(data => {
        this.helpfulAttributes = data;
      });

      this._careerDetailService.getFeaturedProfile(this.mcId).subscribe(data => {
        this.featuredProfile = data;
        this.profile = this.featuredProfile[0];
        this.refreshData();
      });

      this.mcIdLastTwoChar = this.mcId.substr(this.mcId.length - 2);
      this.mcIdTrimmed = this.mcId.slice(0, -3);

      // Did not see where this was used so for now did not replace it
      // this.hotJob = data["data"]["additionalDetails"]["hotJob"];

      // used for stars rating in career-details with login
      this._careerDetailService.getSkillRating(this.mcId).subscribe(data => {
        this.skillRating = data;
      });

      this._careerDetailService.getServiceOfferingCompositeScore(this.mcId).subscribe(data => {
        this.serviceOfferingCompositeScore = data;
      });

      let pMcId = this.mcId.slice(0, -3);
      pMcId = pMcId + '.00';

      this._careerDetailService.getParentSkillRating(pMcId).subscribe(data => {
        this.parentSkillRating = data;
      });

      this._careerDetailService.getParentMilitaryCareer(pMcId).subscribe(data => {
        this.parentMilitaryCareer = data;
      });
      window.scrollTo(0, 0);
    }
  }

  setSearchList() {
    this.allFilter = this.searchList.filter(job => job.mcId.slice(0, -3)
    === this.mcIdTrimmed && job.mcId.substr(job.mcId.length - 3) !== '.00');

    // for (let i = 0; i < this.allFilter.length; i++) {
    //  this.allFilter[i].backgroundImageUrl = 'url(\'' + this.baseImageUrl + 'citm-images/' + this.allFilter[i].mcId + '.jpg\')';
    // }

    for (let i = 0; i < this.searchList.length; i++) {
      if (this.searchList[i].mcId === this.mcId) {
      this.searchListItem = this.searchList[i];
      }
    }

  }

  refreshData() {

    const baseImageUrl = this._config.getImageUrl();
    this.imageUrl = baseImageUrl + 'citm-images/' + this.mcId + '.jpg';
    const fullUrl = location.href;

    if (this.militaryCareer) {
      this.militaryCareer.imageUrl = baseImageUrl + 'citm-images/' + this.militaryCareer.mcId + '.jpg';
      this.careerTitle = 'I found this at Careers in the Military:  ' + this.militaryCareer.title;
      this.careerDescription = 'I\'m exploring ' + this.militaryCareer.title + ' at Careers in the Military.';
      this.shortenDescription = this.getShortenDescription() + '&via=CareersintheMilitary';
      // this._recentlyViewedService.setRecentlyViewed(this.militaryCareer.mcId, this.militaryCareer.title);
      this._titleTag.setTitle(this.militaryCareer.title + ' | Careers in the Military');
      this._meta.updateTag({name: 'description', content: this.militaryCareer.description});

      this._meta.updateTag({property: 'og:title', content: this.militaryCareer.title  + ' | Careers in the Military'});
      this._meta.updateTag({property: 'og:description', content: this.militaryCareer.description});
      this._meta.updateTag({property: 'og:image', content: this.imageUrl});
      this._meta.updateTag({property: 'og:image:alt', content: this.militaryCareer.title});
      this._meta.updateTag({property: 'og:url', content: fullUrl});

      this._meta.updateTag({ name: 'twitter:site', content: '@CareersintheMil'});
      this._meta.updateTag({ name: 'twitter:creator', content: '@CareersintheMil'});
      this._meta.updateTag({ name: 'twitter:card', content: 'summary_large_image'});
      this._meta.updateTag({ name: 'twitter:title', content:  this.militaryCareer.title  + ' | Careers in the Military'});
      this._meta.updateTag({ name: 'twitter:image', content: this.imageUrl});
      this._meta.updateTag({ name: 'twitter:description', content: this.militaryCareer.description});

    }

    // Manipulation of data for Service Offering
    if (this.serviceOffering && !this.gotServiceOffering) {
      this.armyServiceOffering = [];
      this.marineServiceOffering = [];
      this.navyServiceOffering = [];
      this.airForceServiceOffering = [];
      this.coastGuardServiceOffering = [];
      this.nationalGuardServiceOffering = [];
      this.navyScores = false;
      this.airForceScores = false;
      this.gotServiceOffering = true;


      for (let i = 0; i < this.serviceOffering.length; i++) {
        const offering = this.serviceOffering[i];

        switch (this.serviceOffering[i].svc) {
          case ('A'):
            this.armyServiceOffering. push(offering);
            break;
          case ('M'):
            this.marineServiceOffering.push(offering);
            break;
            case ('N'):
            this.navyServiceOffering.push(offering);
            if (this.serviceOffering[i].navyCompScoreOne && this.serviceOffering[i].navyCompScoreOne.length > 0) {
              this.navyScores = true;
            }
            break;
            case ('F'):
            this.airForceServiceOffering.push(offering);
            if (this.serviceOffering[i].compScoreOne) {
              this.airForceScores = true;
            }
            break;
            case ('C'):
            this.coastGuardServiceOffering.push(offering);
            break;
            case ('NG'):
            this.nationalGuardServiceOffering.push(offering);
            break;
        }
      }

      this.armyServiceOffering = this.getDistinctServiceOfferings(this.armyServiceOffering);
      this.marineServiceOffering = this.getDistinctServiceOfferings(this.marineServiceOffering);
      this.navyServiceOffering = this.getDistinctServiceOfferings(this.navyServiceOffering);
      this.airForceServiceOffering = this.getDistinctServiceOfferings(this.airForceServiceOffering);
      this.coastGuardServiceOffering = this.getDistinctServiceOfferings(this.coastGuardServiceOffering);
      this.nationalGuardServiceOffering = this.getDistinctServiceOfferings(this.nationalGuardServiceOffering);

      this.marineScoreData = this.serviceOffering.filter(job => job.svc === 'M' && job.mcId.substr(this.mcId.length - 4) === '.00');
      this.navyScoreData = this.serviceOffering.filter(job => job.svc === 'N' && job.mcId.substr(job.mcId.length - 4) === '.00');
      this.airForceScoreData = this.serviceOffering.filter(job => job.svc === 'F' && job.mcId.substr(this.mcId.length - 4) === '.00');
      this.coastGuardScoreData = this.serviceOffering.filter(job => job.svc === 'C' && job.mcId.substr(this.mcId.length - 4) === '.00');
      this.nationalGuardScoreData = this.serviceOffering.filter(job => job.svc === 'NG' && job.mcId.substr(this.mcId.length - 4) === '.00');
    }

    if (this.serviceOffering && this.armyScore) {
      this.armyScoreData = [];
      for (let i = 0; i < this.armyScore.length; i++) {
        if (this.armyScore[i].mcId.substr(this.serviceOffering[i].mcId.length - 3) === '.00') {
          this.armyScoreData.push(this.armyScore);
        }
      }
    }

//    const baseImageUrl = this._config.getImageUrl();
    if (this.featuredProfile && this.featuredProfile.length > 0) {
      for (let i = 0; i < this.featuredProfile.length; i++) {
        this.featuredProfile[i].imageUrl = baseImageUrl + 'profile-images/' + this.featuredProfile[i].imageList[0].imageName;
      }
    }
  }

  getDistinctServiceOfferings(incomingArray) {
    const distinct = [];
    for (let i = 0; i < incomingArray.length; i++) {
      let found = false;
      for (let i2 = 0; i2 < distinct.length; i2++) {
        if (distinct[i2].mocTitle === incomingArray[i].mocTitle) {
          found = true;
          break;
        }
      }
      if (!found) {
        distinct.push(incomingArray[i]);
      }

    }
    return distinct;
  }

  setCareerTitles() {
    if (this.otherTitles) {
      let titles = '';
      for (let i = 0; i < this.otherTitles.length; i++) {
        if (this.showMore && i > 2) {
          break;
        }
        titles = titles ? titles + ', ' + this.otherTitles[i] : this.otherTitles[i];
      }
      this.otherCareerTitles = titles;
    }
  }

  onSeeScores(type, code) {
    switch (code) {
      case ('A'):
        this.serviceImage = 'army';
        break;
      case ('M'):
        this.serviceImage = 'marines';
        break;
      case ('N'):
        this.serviceImage = 'navy';
        break;
      case ('F'):
        this.serviceImage = 'air-force';
    }
    const scoreObj = {
      serviceName: type,
      mcId: this.mcId,
      serviceId: code,
      title: this.careerTitle,
      imageType: this.serviceImage,
      isLoggedIn: this.isLoggedIn()
    };

    const dialogRef = this._dialog.open(ScoreDialogComponent, {
      data: { scoreObj: scoreObj }
    });

    const self = this;
    dialogRef.afterClosed().subscribe(result => {

      const route =  result.path;
      self._router.navigate([route]);

    });
  }

  onFavoriteClick(item) {
    //  Screen does not have reference on this.searchListItem so did it this way;
    //          if it is a speciality item it works to send the object
    item = item ? item : this.searchListItem;
    this._spinner.show();
    this._favoritesService.onFavoriteClick(item);
    this._spinner.hide();
  }

  onSocialShareClick(app) {
    const params = {
      url: this.getShareUrl(),
      title: this.militaryCareer.title + ' | Careers in the Military',
      description: this.militaryCareer.description,
      shortDescription: this.getShortenDescription(),
      image: this.imageUrl,
      via: 'CareersintheMil'
    };
    this._utility.displaySocialShare(app, params, 'CITM CareerDetail');
  }

  getShareUrl() {
    return window.location.href;
  }

  getTitle() {
    return this.careerTitle;
  }

  getShortenDescription() {
    return this.militaryCareer.description.substr(0, 200) + '...';
  }

  getEmailDescription() {
    return this.careerDescription;
  }

  // Need this at least for email
  trackSocialShare(app) {
    this._utility.trackSocialShare(app, 'CITM CareerDetail');
  }

  onViewAll(type) {
    let list;
    const title = this.militaryCareer.title;

    switch (type) {
      case ('Army'):
        list = this.armyServiceOffering;
        break;
        case ('Marine Corps'):
        list = this.marineServiceOffering;
        break;
        case ('Navy'):
        list = this.navyServiceOffering;
        break;
        case ('Air Force'):
        list = this.airForceServiceOffering;
        break;
        case ('Coast Guard'):
        list = this.coastGuardServiceOffering;
        break;
        case ('National Guard'):
        list = this.nationalGuardServiceOffering;
        break;
    }
    const dialogRef = this._dialog.open( JobTitlesDialogComponent, { hasBackdrop: true,
      data: {type: type, title: title, list: list}
    });

    const self = this;
    dialogRef.afterClosed().subscribe(result => {

      const route = '/career-detail-service/' +  result.rSvcId + '/' + result.rMcId + '/' + result.rMocId;
      self._router.navigate([route]);
  
    });

  }

  onNotesClick() {
    if (this.isLoggedIn()) {
      // Send the type and Occupation Id
      const noteInfo = {
        typeOfNote: 'occupational',
        title: this.militaryCareer.title,
        mcId: this.mcId,
        note: null
      };
      const dialogRef = this._dialog.open( NotesDialogComponent, {
        data: noteInfo,
      });
    } else {
      const dialogRef = this._dialog.open(NeedToLoginDialogComponent, { hasBackdrop: true, disableClose: true, autoFocus: true });
    }
  }

  onCreateAccount() {
    const dialogRef = this._dialog.open( LoginDialogComponent, {
    });
  }

  // Interest code information modal.
  interestCodeInfo(interestCode) {
    let modalOptions;
    switch (interestCode) {
    case 'R':
      modalOptions = {
        headerText : 'Realistic',
        bodyText : '<ul><li>Realistic activities often involve practical, hands-on problems and solutions such as designing, building, and repairing machinery.</li>' +
        '<li>Realistic occupations generally require workers to have physical and mechanical abilities.</li>' +
        '<li>Realistic types generally prefer to work with things (machines, plants, and animals) rather than people. In their work, they are practical, straightforward, and persistent.</li><ul>'
      };
      break;
    case 'I':
      modalOptions = {
        headerText : 'Investigative',
        bodyText : '<ul><li>Investigative activities involve testing ideas, learning new things, and allow you to use your knowledge to solve problems.</li>' +
        '<li>Investigative occupations generally require workers to have math and science abilities.</li>' +
        '<li>Investigative types generally prefer to work with ideas rather than with people or things. In their work, they are curious, observant, and logical.</li><ul>'
      };
      break;
    case 'A':
      modalOptions = {
        headerText : 'Artistic',
        bodyText : '<ul><li>Artistic activities allow you to be creative to do original work such as writing, singing, dancing, sculpting, and painting.</li>' +
        '<li>Artistic occupations generally require workers to have artistic abilities and a good imagination.  </li>' +
        '<li>Artistic types generally prefer to work with ideas rather than things, and like to be in a setting where they can work without a clear set of rules. In their work, they are expressive, original, and independent.</li><ul>'
      };
      break;
    case 'S':
      modalOptions = {
        headerText : 'Social',
        bodyText : '<ul><li>Social activities allow you to use your skills and talents to help, teach, and counsel others.</li>' +
        '<li>Social occupations generally require personal interaction and communication skills and abilities.</li>' +
        '<li>Social types prefer to work with people rather than to work with objects, machines, or data. In their work, they are friendly, outgoing, and helpful.</li><ul>'
      };
      break;
    case 'E':
      modalOptions = {
        headerText : 'Enterprising',
        bodyText : '<ul><li>Enterprising activities allow you to take a leadership role in areas such as sales, supervision, and project or business management.</li>' +
        '<li>Enterprising occupations generally require workers to have leadership, sales, and speaking abilities.</li>' +
        '<li>Enterprising types prefer to work with people and ideas rather than things. They like work that is fast-paced, requires a lot of responsibility and decision making, and involves taking risks for profit. In their work, they are persuasive, ambitious, and energetic.</li><ul>'
      };
      break;
    case 'C':
      modalOptions = {
        headerText : 'Conventional',
        bodyText : '<ul><li>Conventional activities require attention to accuracy and detail such as maintaining orderly and accurate records, procedures, and routines.</li>' +
        '<li>Conventional occupations generally require workers to have clerical, organizational, and arithmetic abilities.</li>' +
        '<li>Conventional types prefer working with data more than with ideas. They apply precise standards in a setting where there is a clear line of authority. In their work, they are efficient, methodical, and detail-oriented.</li><ul>'
      };
      break;
    }

    const data = {
      'title': modalOptions.headerText,
      'message': modalOptions.bodyText
    };
    const dialogRef1 = this._dialog.open(MessageDialogComponent, {
      data: data,
      hasBackdrop: true,
      disableClose: true,
      maxWidth: '400px'
    });

  }

  /**
   * Opens modal popup for description for each section.
   */
  openDescription(section) {

    let headerTitle, bodyText;
    if (section === 'interest-codes') {
      headerTitle = 'Interest Codes';
      bodyText = 'RIASEC codes represent the interest codes <br>most closely related to each occupation. <br>Evaluate how well the interest codes for this <br>occupation match your interests.'
    } else if (section === 'skill-importance') {
      headerTitle = 'Skill Importance Ratings';
      bodyText = 'These ratings show the relative importance <br>of Verbal, Math, and Science/Technical skills <br>for performing the job. The Skill Importance <br>Ratings are not the same as Career <br>Exploration Scores, but can be used to<br> evaluate suitability for the job.';
    } else if (section === 'what-they-do') {
      headerTitle = 'What They Do';
      bodyText = 'This is a list of tasks performed on the job.';
    } else if (section === 'how-to-get-there') {
      headerTitle = 'How to Get There';
      bodyText = 'There may be different ways to get started in the career you want.  Learn about the education, credentials, licenses, apprenticeships, and military opportunities below.';
    } else if (section === 'education') {
      headerTitle = 'Education';
      bodyText = 'This section describes the levels of education seen as necessary to get this job by people who work in the job. You can find a list of institutions that offer degrees related to this occupation under College Info.';
    } else if (section === 'credentials') {
      headerTitle = 'Credentials';
      bodyText = 'Certification, licensure, and/or apprenticeships are possible paths to this career. Select Find Certification, Find Licenses, and Find Apprenticeships to explore these options.';
    } else if (section === 'mil-offering') {
      headerTitle = 'Military Services Offering this Occupation';
      bodyText = 'This job is available in the Military.  Select More Details to learn more about which branches offer related opportunities.';
    } else if (section === 'emp-stats') {
      headerTitle = 'Employment Statistics';
      bodyText = 'This section illustrates the average salary for this occupation by state. Select More Details to learn more about the national employment outlook.';
    } else {
      return 0;
    }

    const data = {
      'title': headerTitle,
      'message': bodyText
    };
    const dialogRef1 = this._dialog.open(MessageDialogComponent, {
      data: data,
      hasBackdrop: true,
      disableClose: true,
      maxWidth: '400px'
    });

  }

  /**
   * Skill rating
   */
  // angular.element(document).ready(function() {
  //   $('.featured-profiles').slick({
  //     dots: true,
  //     infinite: true,
  //     arrows: false,
  //     slidesToScroll: 1,
  //     autoplay: true,
  //     autoplaySpeed: 3500
  //   });
  //   $('.snap-shot-slides').each(function () {
  //     $(this).slick({
  //       dots: false,
  //       infinite: true,
  //       prevArrow: '<button style="width:auto;" type="button" class="slick-prev" role="button"><img src="images/arrow-left-red.png"></button>',
  //       nextArrow: '<button style="width:auto;" type="button" class="slick-next" role="button"><img src="images/arrow-right-red.png"></button>',
  //       slidesToShow: 1,
  //       speed: 1500,
  //       slidesToScroll: 1
  //     })
  //   });
  // })

  // angular;.element(document).ready(function() {
  //   /**
  //    * New star rating.
  //    */
  //   $(function() {
  //       $( '.ratebox' ).raterater({

  //       // allow the user to change their mind after they have submitted a rating
  //       allowChange: false,

  //       // width of the stars in pixels
  //       starWidth: 12,

  //       // spacing between stars in pixels
  //       spaceWidth: 0,

  //       numStars: 5,
  //       isStatic: true,
  //       step: false,
  //       });

  //   });
  // })

  ngOnDestroy() {
    if (this.navigationSubscription) {
        this.navigationSubscription.unsubscribe();
    }
  }
}
