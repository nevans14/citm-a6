import { Injectable } from '@angular/core';

 declare let ga: Function;

@Injectable({
  providedIn: 'root'
})
export class GoogleAnalyticsService {

   constructor() {}

    trackClick(param): void {
        const path = window.location.href;
        ga('send', 'pageview', path + param);
    }

    trackSampleTestClick(param): void {
        const path = window.location.href;
        ga('send', 'pageview', path + param);
    }

    trackSocialShare(socialPlug, value): void {
        const path = window.location.href;
        ga('send', 'event', socialPlug, value);
    }

    trackStudent(userData): void {
        const path = window.location.href;
        if (userData.zipCode) {
            ga('send', 'event', 'StudentLogin', 'ZipCode', userData.zipCode);
        }
        if (userData.city) {
            ga('send', 'event', 'StudentLogin', 'City', userData.city);
        }
        if (userData.state) {
            ga('send', 'event', 'StudentLogin', 'State', userData.state);
        }
        if (userData.mepsId) {
            ga('send', 'event', 'StudentLogin', 'MEPs', userData.mepsId);
        }
        if (userData.mepsName) {
            ga('send', 'event', 'StudentLogin', 'MEPsName', userData.mepsName);
        }
        if (userData.userId) {
            ga('send', 'event', 'StudentLogin', 'UserID', userData.userId);
        }
        if (userData.role) {
            ga('send', 'event', 'StudentLogin', 'Role', userData.role);
        }
    }
}
