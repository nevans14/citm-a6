import { Injectable } from '@angular/core';
import { HttpHelperService } from './http-helper.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PayService {

    constructor(
        private _httpHelper: HttpHelperService,
        private _http: HttpClient
    )  { }


    getPay(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('/option/get-Pay/', null));
    }

    getRank(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('/option/get-rank/', null));
    }

    getPayChart(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('/option/get-pay-chart/', null));
    }
}
