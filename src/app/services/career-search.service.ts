import { Injectable } from '@angular/core';
import { HttpHelperService } from './http-helper.service';
import { HttpClient } from '@angular/common/http';
import { of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CareerSearchService {
  careerClusterListObject: any;

  constructor(
    private _http: HttpClient,
    private _httpHelper: HttpHelperService
  ) { }

  getCareerCluster(): Observable<any> {

    return this._http.get(this._httpHelper.getFullUrl('/search/get-career-cluster/', null));
  }

getCareerClusterPathway (ccId): Observable<any> {
    const params = this.createIdParam(ccId);

    return this._http.get(this._httpHelper.getFullUrl( 'search/get-career-cluster-pathway/' ,  params));
}

  getCareerPath(mcId): Observable<any> {
    const params = this.createIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('detail-page/get-career-path/', params));
  }

  getServiceContacts(): Observable<any> {

    return this._http.get(this._httpHelper.getFullUrl('/search/get-service-contacts/', null));
  }

  createIdParam(id) {
    const params = [];
    params.push({
      key: null,
      value: id
    });
    return params;
  }

  setCareerCluster(careerCluster) {
    this.careerClusterListObject = careerCluster;
  }

}
