import { TestBed, inject } from '@angular/core/testing';

import { CareerDetailService } from './career-detail.service';

describe('CareerDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CareerDetailService]
    });
  });

  it('should be created', inject([CareerDetailService], (service: CareerDetailService) => {
    expect(service).toBeTruthy();
  }));
});
