import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareerDetailServiceComponent } from './career-detail-service.component';

describe('CareerDetailServiceComponent', () => {
  let component: CareerDetailServiceComponent;
  let fixture: ComponentFixture<CareerDetailServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareerDetailServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareerDetailServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
