import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindRecruiterDialogComponent } from './find-recruiter-dialog.component';

describe('FindRecruiterDialogComponent', () => {
  let component: FindRecruiterDialogComponent;
  let fixture: ComponentFixture<FindRecruiterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindRecruiterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindRecruiterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
