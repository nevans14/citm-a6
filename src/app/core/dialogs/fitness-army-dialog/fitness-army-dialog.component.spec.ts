import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitnessArmyDialogComponent } from './fitness-army-dialog.component';

describe('FitnessArmyDiaglogComponent', () => {
  let component: FitnessArmyDialogComponent;
  let fixture: ComponentFixture<FitnessArmyDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitnessArmyDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitnessArmyDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
