import { Component, OnInit, Inject } from '@angular/core';
import { ScoreService } from 'app/services/score.service';
import { ConfigService } from 'app/services/config.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { CareerDetailService } from 'app/services/career-detail.service';
import { PortfolioService } from 'app/services/portfolio.service';
import { LoginDialogComponent } from 'app/core/dialogs/login-dialog/login-dialog.component';

@Component({
    selector: 'app-score-dialog',
    templateUrl: './score-dialog.component.html',
    styleUrls: ['./score-dialog.component.scss']
})

export class ScoreDialogComponent implements OnInit {
    modalOptions: any;
    viewModel: any;
    name: string;
    isDisabled: boolean;
    value: any;
    valid: boolean;
    invalid: boolean;
    pending: boolean;
    disabled: boolean;
    enabled: boolean;
    pristine: boolean;
    dirty: boolean;
    touched: boolean;
    status: string;
    untouched: boolean;
    scoreSummary: any;
    imageTitle;
    serviceContacts: any;
    serviceOffering: any;
    userSystem: any;
    groupIdList: any[];
    dialogOptions: any;
    model: any;
    path: string[];
    formDirective: any;
    scoreObj: any;
    compositeScores: any;
    fa: any;
    fb: any;
    na: any;
    nb: any;
    aa: any;
    ab: any;
    compositeFavoritesFlags: Array<{}> = [];
    mocId: any;
    groupId: any;
    armyOffering: any;
    filterServiceOffering(serviceOffering): any[] {
        const serviceFiltered = serviceOffering.filter(i => i.groupId === this.groupId && i.mocId === this.mocId);
        const sortServiceFiltered = serviceFiltered.sort((a, b) => (a.value > b.value) ? 1 : -1);
        return sortServiceFiltered;
    }
    reset(value?: any): void {
        throw new Error('Method not implemented.');
    }

    constructor(private _congfigService: ConfigService,
		private _scoreService: ScoreService,
        public dialogRef: MatDialogRef<ScoreDialogComponent>,
        private _dialog: MatDialog,
        private _route: ActivatedRoute,
        private _portfolioService: PortfolioService,
        @Inject(MAT_DIALOG_DATA) public data,
        private _careerDetailService: CareerDetailService) {
        this.scoreObj = this.data.scoreObj;
        if(this.scoreObj.isLoggedIn) {
			if(this.scoreObj.serviceName === 'Scores'){
				this.redirect('composite-scores');
			} else {
				this._scoreService.getScore().subscribe(scoreData => {
					this.compositeScores = scoreData['aFQTRawSSComposites'];
					if (this.compositeScores) {
						this._scoreService.setScoreObject(this.compositeScores);
						this.compositeScores.el_cg = this.compositeScores.el_navy;
						this.compositeScores.mr_cg = this.compositeScores.mec_navy;
						this.compositeScores.gt_cg = this.compositeScores.gt_navy;
						this.compositeScores.cl_cg = this.compositeScores.adm_navy;
					}
				});
				this._portfolioService.getUserSystem().subscribe(usersys => {
					this.userSystem = usersys;
				});
				this._scoreService.getServiceOffering(this.scoreObj.mcId).subscribe(svcOffering => {
					this.serviceOffering = svcOffering;
					this.mocId = this.serviceOffering.mocId;
					this.groupId = this.serviceOffering.groupId;
				});
				this._careerDetailService.getArmyScores(this.scoreObj.mcId).subscribe(armyScore => {
					this.armyOffering = armyScore;
					this.groupIdList = [];

					if (this.serviceOffering) {
						for (let i = 0; i < this.serviceOffering.length; i++) {

							let duplicate = false;
							for (let j = 0; j < this.groupIdList.length; j++) {
								if (this.groupIdList[j].groupId === this.serviceOffering[i].groupId
									&& this.groupIdList[j].mocId === this.serviceOffering[i].mocId) {
									duplicate = true;
									this.groupIdList[j].duplicate = duplicate;
								}
							}

							if (!duplicate) {
								this.groupIdList.push({groupId: this.serviceOffering[i].groupId,
									mocId: this.serviceOffering[i].mocId, duplicate: duplicate});
							}
						}
					}
				});

				this._scoreService.getCompositeFavoritesFlags().subscribe(flagData => {
					this.compositeFavoritesFlags = flagData;
				});
			}
		}
    }

    addRemoveFavComposite(compScore, value) {
        if (value === true) {
            this.compositeFavoritesFlags[compScore] = 1;
        } else {
            this.compositeFavoritesFlags[compScore] = 0;
        }
        this._scoreService.setCompositeFavoritesFlags(this.compositeFavoritesFlags);
    }

    // Army composite lookup.
    armyCompositeScores(id) {
        switch (id) {
            case 'GT':
                return 'General Technical';
            case 'CL':
                return 'Clerical';
            case 'CO':
                return 'Combat';
            case 'EL':
                return 'Electronics Repair';
            case 'FA':
                return 'Field Artillery';
            case 'GM':
                return 'General Maintenance';
            case 'MM':
                return 'Mechanical Maintenance';
            case 'OF':
                return 'Operations / Food';
            case 'SC':
                return 'Serveillance / Communications';
            case 'ST':
                return 'Skilled Technical';
        }

    }

    // Army composite property name conversion.
    armyCompositePropertyName(id) {
        switch (id) {
            case 'GT':
                return 'gt_army';
            case 'CL':
                return 'cl_army';
            case 'CO':
                return 'co_army';
            case 'EL':
                return 'el_army';
            case 'FA':
                return 'fa_army';
            case 'GM':
                return 'gm_army';
            case 'MM':
                return 'mm_army';
            case 'OF':
                return 'of_army';
            case 'SC':
                return 'sc_army';
            case 'ST':
                return 'st_army';
        }
    }

    // Army user composite score.
    armyUserCompositeScores(id) {
        switch (id) {
            case 'GT':
                return this.compositeScores.gt_army;
            case 'CL':
                return this.compositeScores.cl_army;
            case 'CO':
                return this.compositeScores.co_army;
            case 'EL':
                return this.compositeScores.el_army;
            case 'FA':
                return this.compositeScores.fa_army;
            case 'GM':
                return this.compositeScores.gm_army;
            case 'MM':
                return this.compositeScores.mm_army;
            case 'OF':
                return this.compositeScores.of_army;
            case 'SC':
                return this.compositeScores.sc_army;
            case 'ST':
                return this.compositeScores.st_army;
        }
    }

    armyCompositeCheckBox = function (id) {
        switch (id) {
            case 'GT':
                return this.compositeFavoritesFlags.gt_army;
            case 'CL':
                return this.compositeFavoritesFlags.cl_army;
            case 'CO':
                return this.compositeFavoritesFlags.co_army;
            case 'EL':
                return this.compositeFavoritesFlags.el_army;
            case 'FA':
                return this.compositeFavoritesFlags.fa_army;
            case 'GM':
                return this.compositeFavoritesFlags.gm_army;
            case 'MM':
                return this.compositeFavoritesFlags.mm_army;
            case 'OF':
                return this.compositeFavoritesFlags.of_army;
            case 'SC':
                return this.compositeFavoritesFlags.sc_army;
            case 'ST':
                return this.compositeFavoritesFlags.st_army;
        }
    };

    // Navy composite lookup.
    navyCompositeScores(id) {
        switch (id) {
            case 'GT':
                return 'General Technical';
            case 'EL':
                return 'Electronics';
            case 'BEE':
                return 'Basic Electricity & Electronics';
            case 'ENG':
                return 'Engineering';
            case 'MEC':
                return 'Mechanical';
            case 'MEC2':
                return 'Mechanical 2';
            case 'NUC':
                return 'Nuclear Field';
            case 'OPS':
                return 'Operations';
            case 'HM':
                return 'Hospitalman';
            case 'ADM':
                return 'Administration';
        }

    }

    // Navy composite property name conversion.
    navyCompositePropertyName(id) {
        switch (id) {
            case 'GT':
                return 'gt_navy';
            case 'EL':
                return 'el_navy';
            case 'BEE':
                return 'bee_navy';
            case 'ENG':
                return 'eng_navy';
            case 'MEC':
                return 'mec_navy';
            case 'MEC2':
                return 'mec2_navy';
            case 'NUC':
                return 'nuc_navy';
            case 'OPS':
                return 'ops_navy';
            case 'HM':
                return 'hm_navy';
            case 'ADM':
                return 'adm_navy';
        }
    }

        // Navy user composite score.
    navyUserCompositeScores(id) {
        switch (id) {
            case 'GT':
                return this.compositeScores.gt_navy;
            case 'EL':
                return this.compositeScores.el_navy;
            case 'BEE':
                return this.compositeScores.bee_navy;
            case 'ENG':
                return this.compositeScores.eng_navy;
            case 'MEC':
                return this.compositeScores.mec_navy;
            case 'MEC2':
                return this.compositeScores.mec2_navy;
            case 'NUC':
                return this.compositeScores.nuc_navy;
            case 'OPS':
                return this.compositeScores.ops_navy;
            case 'HM':
                return this.compositeScores.hm_navy;
            case 'ADM':
                return this.compositeScores.adm_navy;
        }
    }

    navyCompositeCheckBox = function (id) {
        switch (id) {
            case 'GT':
                return this.compositeFavoritesFlags.gt_navy;
            case 'EL':
                return this.compositeFavoritesFlags.el_navy;
            case 'BEE':
                return this.compositeFavoritesFlags.bee_navy;
            case 'ENG':
                return this.compositeFavoritesFlags.eng_navy;
            case 'MEC':
                return this.compositeFavoritesFlags.mec_navy;
            case 'MEC2':
                return this.compositeFavoritesFlags.mec2_navy;
            case 'NUC':
                return this.compositeFavoritesFlags.nuc_navy;
            case 'OPS':
                return this.compositeFavoritesFlags.ops_navy;
            case 'HM':
                return this.compositeFavoritesFlags.hm_navy;
            case 'ADM':
                return this.compositeFavoritesFlags.adm_navy;
        }
    };

    afCompositeScores = function (id) {
        switch (id) {
            case 'M':
                return 'Mechanical';
            case 'A':
                return 'Administrative';
            case 'G':
                return 'General';
            case 'E':
                return 'Electronic';
        }
    };

    afCompositePropertyName(id) {
        switch (id) {
            case 'M':
                return 'm_af';
            case 'A':
                return 'a_af';
            case 'G':
                return 'g_af';
            case 'E':
                return 'e_af';
        }
    }

    afUserCompositeScores = function (id) {
        switch (id) {
            case 'M':
                return this.compositeScores.m_af;
            case 'A':
                return this.compositeScores.a_af;
            case 'G':
                return this.compositeScores.g_af;
            case 'E':
                return this.compositeScores.e_af;
        }
    };

    afCompositeCheckBox = function (id) {
        switch (id) {
            case 'M':
                return this.compositeFavoritesFlags.m_af;
            case 'A':
                return this.compositeFavoritesFlags.a_af;
            case 'G':
                return this.compositeFavoritesFlags.g_af;
            case 'E':
                return this.compositeFavoritesFlags.e_af;
        }
    };
    ngOnInit = function () {


    };

    redirect(path) {
        const returnData = {
            path : path
          };
          this.dialogRef.close(returnData);
    }

    close() {
        this.dialogRef.close();
    }

    openLoginDialog() {
        const dialogRef = this._dialog.open( LoginDialogComponent, { hasBackdrop: true, disableClose: true, autoFocus: true } );
        this.close();
    }

    serviceContactUsModal(service) {
        for (let i = 0; i < this.serviceContacts.length; i++) {
            if (service === this.serviceContacts[i].name) {
                break;
            }
        }
    }
}
