import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacySecurityDialogComponent } from './privacy-security-dialog.component';

describe('PrivacySecurityDialogComponent', () => {
  let component: PrivacySecurityDialogComponent;
  let fixture: ComponentFixture<PrivacySecurityDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivacySecurityDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivacySecurityDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
