import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitnessAirForceDialogComponent } from './fitness-air-force-dialog.component';

describe('FitnessAirForceDialogComponent', () => {
  let component: FitnessAirForceDialogComponent;
  let fixture: ComponentFixture<FitnessAirForceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitnessAirForceDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitnessAirForceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
