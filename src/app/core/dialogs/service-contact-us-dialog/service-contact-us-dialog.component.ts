import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { HttpHelperService } from '../../../services/http-helper.service';

@Component({
    selector: 'app-service-contact-us-dialog',
    templateUrl: './service-contact-us-dialog.component.html',
    styleUrls: ['./service-contact-us-dialog.component.scss']
})
export class ServiceContactUsDialogComponent implements OnInit {

    serviceFormGroup: FormGroup;
    service;
    userType: String = "";
    userTypeOptions = [
        { id: 1, name: "---Select One---", value: "" },
        { id: 2, name: "Student", value: "Student" },
        { id: 3, name: "Parent", value: "Parent" },
        { id: 4, name: "Educator", value: "Educator" },
        { id: 5, name: "Other", value: "Other" }
    ];
    firstName;
    lastName;
    email;
    message;
    city;
    state;
    zip;
    contactType = 1;
    asvabParticipant;
    formDisabled = false;
    recaptchaResponse = '';
    recaptchaKey = '6LcGnigTAAAAAJUUd9llQh4I5r_-j6mlJoAryekN';
    validation;

    constructor(public dialogRef: MatDialogRef<ServiceContactUsDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data,
        private formBuilder: FormBuilder,
        private _httpHelper: HttpHelperService) { }

    ngOnInit() {
        this.service = this.data;
        this.serviceFormGroup = this.formBuilder.group({
            recaptcha: ['', Validators.required]
        });
    }

    close() {
        this.dialogRef.close();
    }

    emailUs = function() {
        this.validation = undefined;
        this.formDisabled = true;
        var emailObject = {
            userType: this.userType,
            asvabParticipant: this.asvabParticipant ? 'Yes' : 'No',
            firstName: this.firstName,
            lastName: this.lastName,
            email: this.email,
            message: this.message,
            city: this.city,
            state: this.state,
            zip: this.zip,
            contactType: this.contactType,
            recaptchaResponse: this.serviceFormGroup.value.recaptcha,
            svcList: [this.service.serviceCode]
        }

        if (this.userType == undefined || this.asvabParticipant == undefined || this.firstName == undefined || this.lastName == undefined || this.email == undefined || this.message == undefined) {
            this.validation = "Fill out all fields.";
            this.formDisabled = false;
            return false;
        }

        if (this.serviceFormGroup.value.recaptcha === "") {
            this.validation = "Please resolve the captcha and submit!";
            this.formDisabled = false;
            return false;
        } else {
            this._httpHelper.httpHelper('POST', '/contactUs/service-contact-us', null, emailObject)
                .then(
                (val) => {
                    this.userType = undefined;
                    this.asvabParticipant = undefined;
                    this.firstName = undefined;
                    this.lastName = undefined;
                    this.email = undefined;
                    this.message = undefined;
                    this.city = undefined;
                    this.state = undefined;
                    this.zip = undefined;
                    this.recaptchaResponse = '';
                    this.formDisabled = false;
                    alert("Request Sent"); //TODO: custom dialog
                },
                (err) => { alert("Request Error " + err); }
                );

        }

    }

    openNewTab(url) {
        window.open(url, '_blank');
    }
}
