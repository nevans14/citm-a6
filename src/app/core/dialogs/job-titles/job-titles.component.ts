import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import {  MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

export interface JobTitlesDialogData {
  type: string;
  title: string;
  list: any;
  rSvcId: any; 
  rMcId: any; 
  rMocId: any;
}

@Component({
  selector: 'app-job-titles',
  templateUrl: './job-titles.component.html',
  styleUrls: ['./job-titles.component.scss']
})
export class JobTitlesDialogComponent implements OnInit {
  serviceOfferings: any;

  constructor(
    public _dialogRef: MatDialogRef<JobTitlesDialogComponent>,
    private _router: Router,
    @Inject(MAT_DIALOG_DATA) public data: JobTitlesDialogData
    ) {}

  ngOnInit() {
    this.serviceOfferings = this.data.list;
  }

  onClickLink(svc, mcId, mocId){
    const returnData = {
      rSvcId : svc,
      rMcId: mcId,
      rMocId: mocId
    };
    this._dialogRef.close(returnData);
  }

  onNoClick(): void {
    this._dialogRef.close();
  }
}
