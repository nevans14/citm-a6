import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobTitlesDialogComponent } from './job-titles.component';

describe('JobTitlesDialogComponent', () => {
  let component: JobTitlesDialogComponent;
  let fixture: ComponentFixture<JobTitlesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobTitlesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobTitlesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
