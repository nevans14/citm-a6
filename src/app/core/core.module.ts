import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { JobTitlesDialogComponent } from 'app/core/dialogs/job-titles/job-titles.component';
import { AdvancedSearchService } from 'app/services/advanced-search.service';
import { CareerDetailService } from 'app/services/career-detail.service';
import { CareerSearchService } from 'app/services/career-search.service';
import { ContentManagementService } from 'app/services/content-management.service';
import { FavoritesService } from 'app/services/favorites.service';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { GuidedExplorationResolveService } from 'app/services/guided-exploration-resolve.service';
import { HttpHelperService } from 'app/services/http-helper.service';
import { LoginService } from 'app/services/login.service';
import { PayService } from 'app/services/pay.service';
import { PortfolioService } from 'app/services/portfolio.service';
import { RankService } from 'app/services/rank.service';
import { RecentlyViewedService } from 'app/services/recently-viewed.service';
import { ScoreService } from 'app/services/score.service';
import { SearchListService } from 'app/services/search-list.service';
import { SessionService } from 'app/services/session.service';
import { UserService } from 'app/services/user.service';
import { NgxCaptchaModule } from 'ngx-captcha';
import { YoutubePlayerModule } from 'ngx-youtube-player';
import { MaterialModule } from '../material.module';
import { FindRecruiterDialogComponent } from './dialogs/find-recruiter-dialog/find-recruiter-dialog.component';
import { FitnessAirForceDialogComponent } from './dialogs/fitness-air-force-dialog/fitness-air-force-dialog.component';
import { FitnessArmyDialogComponent } from './dialogs/fitness-army-dialog/fitness-army-dialog.component';
import { FitnessCoastGuardDialogComponent } from './dialogs/fitness-coast-guard-dialog/fitness-coast-guard-dialog.component';
import { FitnessMarineDialogComponent } from './dialogs/fitness-marine-dialog/fitness-marine-dialog.component';
import { FitnessNavyDialogComponent } from './dialogs/fitness-navy-dialog/fitness-navy-dialog.component';
import { MessageDialogComponent } from './dialogs/message-dialog/message-dialog.component';
import { MilitaryInfoDialogComponent } from './dialogs/military-info-dialog/military-info-dialog.component';
import { NeedToLoginDialogComponent } from './dialogs/need-to-login-dialog/need-to-login-dialog.component';
import { NotesDialogComponent } from './dialogs/notes-dialog/notes-dialog.component';
import { PayDialogComponent } from './dialogs/pay-dialog/pay-dialog.component';
import { PopupPlayerComponent } from './dialogs/popup-player/popup-player.component';
import { PrivacySecurityDialogComponent } from './dialogs/privacy-security-dialog/privacy-security-dialog.component';
import { RankDialogComponent } from './dialogs/rank-dialog/rank-dialog.component';
import { SaveSearchComponent } from './dialogs/save-search/save-search.component';
import { ScoreDialogComponent } from './dialogs/score-dialog/score-dialog.component';
import { ServiceContactUsDialogComponent } from './dialogs/service-contact-us-dialog/service-contact-us-dialog.component';
import { UndoDialogComponent } from './dialogs/undo-dialog/undo-dialog.component';
import { DynamicContentLoaderComponent } from './dynamic-content-loader/dynamic-content-loader.component';
import { FilterPipe } from './filter.pipe';
import { HttpErrorInterceptor } from './http-error-interceptor';
import { SafePipe } from './safe.pipe';
import { RankPipe } from './rank.pipe';

@NgModule({
  imports: [
    MaterialModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    YoutubePlayerModule,
    HttpClientModule,
    CommonModule
  ],
  exports: [
    YoutubePlayerModule,
    SafePipe,
    FilterPipe,
    RankPipe,
    DynamicContentLoaderComponent
  ],

  declarations: [
    PopupPlayerComponent,
    PrivacySecurityDialogComponent,
    PayDialogComponent,
    FitnessAirForceDialogComponent,
    FitnessArmyDialogComponent,
    FitnessCoastGuardDialogComponent,
    FitnessMarineDialogComponent,
    FitnessNavyDialogComponent,
    MilitaryInfoDialogComponent,
    RankDialogComponent,
    JobTitlesDialogComponent,
    NeedToLoginDialogComponent,
    ServiceContactUsDialogComponent,
    NotesDialogComponent,
    MessageDialogComponent,
    SaveSearchComponent,
    ScoreDialogComponent,
    UndoDialogComponent,
    SafePipe,
    FilterPipe,
    DynamicContentLoaderComponent,
    FindRecruiterDialogComponent,
    RankPipe,
  ],

  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    AdvancedSearchService,
    CareerDetailService,
    CareerSearchService,
    HttpHelperService,
    LoginService,
    PayService,
    RankService,
    RecentlyViewedService,
    SearchListService,
    SessionService,
    ContentManagementService,
    FavoritesService,
    GoogleAnalyticsService,
    GuidedExplorationResolveService,
    PortfolioService,
    ScoreService,
    UserService
  ],
  entryComponents: [
    PopupPlayerComponent,
    PrivacySecurityDialogComponent,
    PayDialogComponent,
    FitnessAirForceDialogComponent,
    FitnessArmyDialogComponent,
    FitnessCoastGuardDialogComponent,
    FitnessMarineDialogComponent,
    FitnessNavyDialogComponent,
    MilitaryInfoDialogComponent,
    RankDialogComponent,
    JobTitlesDialogComponent,
    NeedToLoginDialogComponent,
    ServiceContactUsDialogComponent,
    NotesDialogComponent,
    MessageDialogComponent,
    SaveSearchComponent,
    ScoreDialogComponent,
    UndoDialogComponent,
    FindRecruiterDialogComponent
  ]
})
export class CoreModule {}
