import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareerClusterComponent } from './career-cluster.component';

describe('CareerClusterComponent', () => {
  let component: CareerClusterComponent;
  let fixture: ComponentFixture<CareerClusterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareerClusterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareerClusterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
