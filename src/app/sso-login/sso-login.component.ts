import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilityService } from 'app/services/utility.service';
import { ConfigService } from 'app/services/config.service';

@Component({
  selector: 'app-sso-login',
  templateUrl: './sso-login.component.html',
  styleUrls: ['./sso-login.component.scss']
})
export class SsoLoginComponent implements OnInit {
  action: string;
  redirectPage: string;
  data: string;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _utilityService: UtilityService,
    private _config: ConfigService
  ) { }

  ngOnInit() {
    this.action = this._router.url.indexOf('/sso-login') > -1 ? 'login' : 'logoff';
  if (this.action === 'login') {
    this.ssoLogin();
  } else {
    this.ssoLogoff();
  }
    
  this._route.params.subscribe( res => {
    this.redirectPage = res.redirect;
    this.data = res.data;
} );

 }

 ssoLogin() {
   console.debug('Pocess SSO Login');
    if (this.data && this.data !== 'null') {
      const now = new Date();
      const cookieData = JSON.parse(this._utilityService.urlDecode(this.data));
      const userId = cookieData.currentUser.userName;
      const role = cookieData.currentUser.role; 
      const authdata = this._utilityService.encodeData(( userId + ':' + 'null' + ':' + now.toISOString() + ':SSO'));
      const globals = {
        currentUser: {
            role: role,
            authdata: authdata
        }
    };

      this._utilityService.createCookie(globals);
    }
    window.location.href = this._config.getCepBaseUrl() + this.redirectPage;
 }

 ssoLogoff() {
   console.debug('Process SSO Logoff');
   this._config.logOff(false);
   window.location.href = this._config.getCepBaseUrl() + 'home';
 }

}
