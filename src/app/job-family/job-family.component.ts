import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SearchListService } from 'app/services/search-list.service';
import {GoogleAnalyticsService} from '../services/google-analytics.service';
import { Title, Meta } from '@angular/platform-browser';
import { filter } from 'rxjs/operators';

@Component( {
    selector: 'app-job-family',
    templateUrl: './job-family.component.html',
    styleUrls: ['./job-family.component.scss']
} )
export class JobFamilyComponent implements OnInit {
    searchListInterval: any;

    constructor(
        private route: ActivatedRoute,
        private _searchService: SearchListService,
        private _meta: Meta,
        private _titleTag: Title,
        private _googleAnalyticsService: GoogleAnalyticsService
        ) { }



    jobFamilyData: any;
    allJobList: any;

    ngOnInit() {
        this._titleTag.setTitle('Job Family | Careers in the Military');
        this._meta.updateTag({ name: 'description', content: 'Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests.' });

        this._meta.updateTag({ property: 'og:title', content: 'Job Family | Careers in the Military' });
        this._meta.updateTag({ property: 'og:description', content: 'Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests.' });
        this._meta.updateTag({ property: 'og:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });

        this._meta.updateTag({ name: 'twitter:title', content: 'Job Family | Careers in the Military' });
        this._meta.updateTag({ name: 'twitter:description', content: 'Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests.' });
        this._meta.updateTag({ name: 'twitter:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });

        this._searchService.getSearchListObsv().subscribe(searchList => {
            this.allJobList = searchList;
        });

        this._searchService.getJobFamily().subscribe(data => {
            this.jobFamilyData = data;
        });

    }

    ga(param) {
        this._googleAnalyticsService.trackClick(param);
    }

    gaSocialShare(socialPlug, value) {
        this._googleAnalyticsService.trackSocialShare(socialPlug, value);
    }

    filterMilitaryCareer(list, jobFamilyId) {
        return list.filter( item => item.jobFamilyId == jobFamilyId );
    }

}
